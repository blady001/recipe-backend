# README #

### Frontend repo link ###
https://github.com/204Constie/recipe-book

### Wymagania ###
* MongoDB
* Node.js & npm

### Instalacja ###
* W folderze z projektem należy wykonać komendę ```npm install```

### Endpointy ###

#### Unprotected (niewymagające tokena) ####
**[POST] /api/auth/signin**
W body jako x-www-form-urlencoded muszą się znaleźć wartości dla następujących kluczy:

* username
* password

Zwraca JSONa z następującymi kluczami
* success - wartość boolean
* message
* token - tylko w przypadku success = true

**[POST] /api/users/create**
W body jako x-www-form-urlencoded muszą się znaleźć wartości dla następujących kluczy:

* username
* password
* firstname
* lastname
* email
Zwraca JSON z kluczem success

**[GET] /api/recipes/**
Zwraca listę przepisów

**[POST] /api/recipes/:recipeId**
Zwraca JSON z następującymi kluczami:

* succcess - boolean
* recipe - przepis, jeśli success = true

#### Protected - token ustawiony jako nagłówek 'x-access-token' ####
**[GET] /api/users/**
Zwraca listę userów

**[GET] /api/users/:userId**
Zwraca JSON z następującymi kluczami:

* success - boolean
* user - jeśli success = true;

**[POST] /api/users/:userId**
Modifikuje dane danego usera. Parametry trzeba podać jako x-www-form-urlencoded:

* firstname
* lastname
* email
Opcjonalnie - password

**[POST] /api/recipes/create**
W body jako x-www-form-urlencoded muszą się znaleźć wartości dla następujących kluczy:

* name
* description
* imagePath
* ingredients - [object Object]
Zwraca JSON z kluczem success

**[POST] /api/users/:userId**
Modifikuje dany przepis. Parametry trzeba podać jako x-www-form-urlencoded:

* name
* description
* imagePath
* ingredients - [object Object]
Opcjonalnie - password