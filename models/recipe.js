
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var RecipeSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: "Recipe name cannot be blank"
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    imagePath: String /*bitmapa? ścieżka?*/,
    ingredients: [{}],
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Recipe', RecipeSchema);
