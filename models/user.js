/**
 * Created by macblady on 16.05.2017.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var User = new Schema({
    username: String,
    password: String,
    firstname: String,
    lastname: String,
    email: String,
    admin: Boolean
});

module.exports = mongoose.model('User', User);
