/**
 * Created by macblady on 16.05.2017.
 */

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var config = require('../config');
var jwt = require('jsonwebtoken');


router.post('/signin', function(req, res) {
    User.findOne({
        username: req.body.username
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        }
        else if (user) {
            if (user.password != req.body.password) {
                res.json({ success: false, message: 'Authentication failed. Wrong password'});
            }
            else {
                var token = jwt.sign(user, config.secret, {
                    expiresIn: 60*60*24 // in seconds - 24h
                });

                res.json({
                    success: true,
                    message: 'User authenticated',
                    user: user,
                    token: token
                });
            }
        }
    });
});

// TODO: Create logout? - invalidating tokens https://stackoverflow.com/questions/21978658/invalidating-json-web-tokens

module.exports = router;
