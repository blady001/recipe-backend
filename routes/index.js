var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// helper view to create admin user (instead of clicking it)
router.get('/setup-admin', function(req,res) {
  var adminUser = new User({
    username: 'admin',
    password: 'admin',
    firstname: 'Admin',
    lastname: 'Admin',
    email: 'admin@admin.com',
    admin: true
  });

  adminUser.save(function(err) {
    if (err) throw err;

    console.log("Admin user created succesfully!");
    res.json({success: true});
  });
});

module.exports = router;
