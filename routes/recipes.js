/**
 * Created by tomas on 17.05.2017.
 */
var express = require('express');
var router = express.Router();
var Recipe = require('../models/recipe');
var verifyToken = require('../middlewares/verifytoken');


/* GET recipes listing */
router.get('/', function(req,res, next){
    Recipe.find({})
        .populate('user')
        .exec(function(err, recipes){
          if(err){
              throw err;
          }
          res.json(recipes);
        })
});

router.get('/:recipeId', function(req, res){
   Recipe.findById(req.params.recipeId)
       .populate('user')
       .exec(function(err, recipe){
       if(err){
           return res.status(404).json({
               success: false,
               error: err
           })
       }

        return res.json({
           success: true,
           recipe: recipe
       })
   });
});

router.get('/:userId', function(req,res){
    var ObjectId = require('mongoose').Types.ObjectId;
    var query = { user: new ObjectId(req.params.userId) };

    Recipe.find(query)
        .populate('user')
        .exec(function(err, recipes){
           if(err){
               throw err;
           }
           res.json(recipes);
        });
});

router.post('/create', function(req, res) {
    var recipe = new Recipe({
        name: req.body.name,
        description: req.body.description,
        imagePath: req.body.imagePath,
        ingredients: req.body.ingredients,
        user: req.body.user
    });

    recipe.save(function(err) {
        if (err) throw err;

        res.json({success:true});
    });
});

router.post('/:recipeId', function(req, res) {
    var condition = {_id: req.params.recipeId};

    var updateClause = {
        name: req.body.name,
        description: req.body.description,
        imagePath: req.body.imagePath,
        ingredients: req.body.ingredients,
        user: req.body.user
    };

    Recipe.findOneAndUpdate(condition, updateClause, null, function(err, recipe) {
        if (err) throw err;

        res.json({ success: true });
    });
});

router.delete('/:recipeId', function(req, res){
    Recipe.findByIdAndRemove(req.params.recipeId, function(err,  recipe){
        if(err){
            throw err;
        }

        var response = {
            message: "successfully deleted recipe",
            id: recipe._id
        };
        res.json(response);
    });
});

module.exports = router;