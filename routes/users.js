var express = require('express');
var router = express.Router();
var User = require('../models/user');
var verifyToken = require('../middlewares/verifytoken');
var jwt = require('jsonwebtoken');
var config = require('../config');

/* GET users listing. */
router.post('/create', function(req, res) {
  var user = new User({
    username: req.body.username,
    password: req.body.password,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    admin: false
  });

  user.save(function(err) {
    if (err) throw err;

    res.json({success:true});
  });
});
router.get('/', function(req, res, next) {
  User.find({}, function(err, users) {
    if (err) throw err;

    res.json(users);
  });
});

router.get('/:userId', verifyToken, function(req, res) {
  User.findById(req.params.userId, function(err, user) {
    if (err)
      res.status(404).json({ success: false });
    else
      res.json({ success: true, 'user': user });
  });
});

router.post('/:userId', verifyToken, function(req, res) {
  var condition = {_id: req.params.userId};

  var updateClause = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email
  };

  if (req.body.password && req.body.password != '')
    updateClause.password = req.body.password;

  User.findOneAndUpdate(condition, updateClause, null, function(err, user) {
    if (err) throw err;

    res.json({ success: true });
  });
});

// router.post('/create', function(req, res) {
//   var user = new User({
//     username: req.body.username,
//     password: req.body.password,
//     firstname: req.body.firstname,
//     lastname: req.body.lastname,
//     email: req.body.email,
//     admin: false
//   });
//
//   user.save(function(err) {
//     if (err) throw err;
//
//     res.json({success:true});
//   });
// });


module.exports = router;
